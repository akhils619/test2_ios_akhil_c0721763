//
//  Infection.swift
//  ScreamItus-IOS
//
//  Created by parrot on 2019-09-19.
//  Copyright © 2019 room1. All rights reserved.
//

import Foundation
class Infection {
    
    init(){}
    
    func calculateTotalInfectedTest(day: Int) -> Int {
        var result: Int = 0
        if day <= 0 {
            result =  -1;
        }else if day > 7{
            result = (day-7)*8 + 30
            
        }else {
            result = day * 5;
        }
        return result;
    }
    
    
    
}
