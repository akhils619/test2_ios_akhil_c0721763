//
//  ScreamItus_IOSTests.swift
//  ScreamItus-IOSTests
//
//  Created by Akhil S Raj on 2019-09-19.
//  Copyright © 2019 room1. All rights reserved.
//

import XCTest

@testable import ScreamItus-IOS

class InfectionTest: XCTestCase {
    
    var infection:Infection!

    override func setUp() {
        infection = Infection()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    //R1
    func daysGreaterThan0Test(){
        let result:Int = infection.calculateTotalInfected(0)
        XCTAssertEqual(result, -1)
    }
    //R2
    func infectionRateTest(){
        let numberOFInfectedPerDay: Int = 5
        let numberOfDays:Int = 3
        let numberOfInfected = infection.calculateTotalInfected(numberOfDays)
        XCTAssertEqual(15, numberOfInfected)
        
    }

    //R3
    func daysGreaterThan7Test() {
        let numberOFInfectedPerDay: Int = 5
        let numberOfDays:Int = 9
        let numberOfInfected = infection.calculateTotalInfected(numberOfDays)
        XCTAssertEqual(51, numberOfInfected)
        
    }
    
    //R4
    func evenDaysInfectionRateTest(){
        let numberOFInfectedPerDay: Int = 5
        let numberOfDays:Int = 10
        let numberOfInfected = infection.calculateTotalInfected(numberOfDays)
        XCTAssertEqual(28, numberOfInfected)
    }
}
